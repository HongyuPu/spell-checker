import java.io.File;
import java.lang.reflect.Array;
import java.util.Scanner;
import java.util.ArrayList;

public class SpellChecker {
    public static void main(String[] args) {
        ArrayList<String> dictionary = new ArrayList<>(); //array list to put dictiocatnary into
        try { //try and catch block to read and put words into array
            java.util.Scanner input = new java.util.Scanner(new java.io.File("Dictionary.txt"));
            while (input.hasNext()) {
                String line = input.next();
                dictionary.add(line.toLowerCase()); //lowercases all
            }
        }
        catch (java.io.IOException ex) {
            System.out.println("File exception: " + ex.getMessage()); //exception
        }

        BinarySearchTree<String> dictionaryTree = new BinarySearchTree<>(); //new binary search tree object
        java.util.Collections.shuffle(dictionary, new java.util.Random(System.currentTimeMillis())); //randomized array of words



        for (String i : dictionary) { //puts dictionary words into tree
            dictionaryTree.insert(i);
        }

        ArrayList<String> letter = new ArrayList<>(); //array list for the words in letter
        try {
            java.util.Scanner input = new java.util.Scanner(new java.io.File("Letter.txt")); //reads the letter
            while (input.hasNext()) {
                String line = input.next();
                letter.add(line.replaceAll("\\p{Punct}", "").toLowerCase()); //gets rid of punctuation and lowercases all
            }
        }
        catch (java.io.IOException ex) {
            System.out.println("File exception: " + ex.getMessage()); //exception
        }

        System.out.println("--- Misspelled Words ---"); //prints out the misspelled words
        for (String i : letter) {
            if (!dictionaryTree.search(i)) {
                System.out.println(i);
            }
        }

        System.out.println("-- Tree Stats --"); //stats for the dictionary binary tree
        System.out.println("Total Nodes: " + dictionaryTree.numberNodes()); //prints out the number of nodes
        System.out.println("Leaf Nodes: " + dictionaryTree.numberLeafNodes()); //prints out the number of leaf nodes
        System.out.println("Tree Height: " + dictionaryTree.height()); //prints out the height

        testTree();
    }

    public static void testTree() {
        BinarySearchTree<String> tree = new BinarySearchTree<>();

        //
        // Add a bunch of values to the tree
        tree.insert("Olga");
        tree.insert("Tomeka");
        tree.insert("Benjamin");
        tree.insert("Ulysses");
        tree.insert("Tanesha");
        tree.insert("Judie");
        tree.insert("Tisa");
        tree.insert("Santiago");
        tree.insert("Chia");
        tree.insert("Arden");

        //
        // Make sure it displays in sorted order
        tree.display("--- Initial Tree State ---");
        reportTreeStats(tree);

        //
        // Try to add a duplicate
        if (tree.insert("Tomeka")) {
            System.out.println("oops, shouldn't have returned true from the insert");
        }
        tree.display("--- After Adding Duplicate ---");
        reportTreeStats(tree);

        //
        // Remove some existing values from the tree
        tree.remove("Olga");    // Root node
        tree.remove("Arden");   // a leaf node
        tree.display("--- Removing Existing Values ---");
        reportTreeStats(tree);

        //
        // Remove a value that was never in the tree, hope it doesn't crash!
        tree.remove("Karl");
        tree.display("--- Removing A Non-Existent Value ---");
        reportTreeStats(tree);
    }

    public static void reportTreeStats(BinarySearchTree<String> tree) {
        System.out.println("-- Tree Stats --");
        System.out.printf("Total Nodes : %d\n", tree.numberNodes());
        System.out.printf("Leaf Nodes  : %d\n", tree.numberLeafNodes());
        System.out.printf("Tree Height : %d\n", tree.height());
    }

}
