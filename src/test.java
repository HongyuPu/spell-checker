import java.io.IOException;
import java.util.Scanner;
import java.io.File;
import java.util.ArrayList; //put list in array, randomize it or start from middle, then put into binary tree

public class test {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        try {
            java.util.Scanner input = new java.util.Scanner(new java.io.File("Letter.txt"));
            while (input.hasNext()) {
                String line = input.next();
                list.add(line.replaceAll("\\p{Punct}", "").toLowerCase()); //gets rid of puncutation and lowercases all
            }
        }
        catch (java.io.IOException ex) {
            System.out.println("File exception: " + ex.getMessage());
        }

        BinarySearchTree<String> treeTest = new BinarySearchTree<>();
        java.util.Collections.shuffle(list, new java.util.Random(System.currentTimeMillis()));

        for (String i : list) {
//            System.out.println(i);
            treeTest.insert(i);
        }

        treeTest.display("Start of Print");
    }
}
