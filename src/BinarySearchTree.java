public class BinarySearchTree<E extends Comparable<E>> {
    private TreeNode root;
    private int numberOfNodes;
    private int numberOfLeafNodes;
    private int height;

    public class TreeNode { //TreeNode class
        public E value;
        public TreeNode left; //left and right references
        public TreeNode right;

        public TreeNode(E value) {
            this.value = value;
        }
    }


    public boolean insert(E value) {
        if (root == null) {
            root = new TreeNode(value); //if tree is null condition
        }

        else {
            TreeNode parent = null;
            TreeNode node = root;

            while (node != null) { //makes sure there are not duplicates
                parent = node;
                if (node.value.compareTo(value) < 0) { //moves to correct refereces and
                    node = node.right;
                }
                else if (node.value.compareTo(value) > 0){
                    node = node.left;
                }
                else {
                    return false; //if node references same value then return false
                }
            }

            TreeNode newNode = new TreeNode(value);
            if (parent.value.compareTo(value) < 0) { //puts value into tree
                parent.right = newNode;
            }
            else {
                parent.left = newNode;
            }
        }

        numberOfNodes++; //increases by one to eep track of the number of nodes
        return true; //return true if it made it through the method
    }

    public boolean remove(E value) {
        TreeNode parent = null;
        TreeNode node = root;

        while (node != null) {
            if (node.value.compareTo(value) < 0) {
                parent = node;
                node = node.right;
            }
            else if (node.value.compareTo(value) > 0) {
                parent = node;
                node = node.left;
            }
            else {
                break; //element is in the tree, node = element
            }
        }

        if (node == null) { //element not in tree
            return false;
        }

        if (node.left == null) { //case for no left child
            if (parent == null) {
                root = node.right; //connect the parent with the right child of the current node
            }
            else {
                if (parent.value.compareTo(value) < 0) {
                    parent.right = node.right;
                }
                else {
                    parent.left = node.right;
                }
            }
        }
        else { //case where the node has a left child
            TreeNode parentOfRight = node;
            TreeNode rightMost = node.left;

            while (rightMost.right != null) {
                parentOfRight = rightMost;
                rightMost = rightMost.right;
            }

            node.value = rightMost.value;

            if (parentOfRight.right == rightMost) {
                parentOfRight.right = rightMost.left;
            }
            else {
                parentOfRight.left = rightMost.left;
            }
        }

        numberOfNodes--; //Decreased by one to keep track of number of nodes
        return true; //if it made it through the whole method
    }

    public boolean search(E value) {
        boolean found = false; //sets initially false
        TreeNode node = root;

        while (!found && node != null) {
            if (node.value.equals(value)) { //sets true if found
                found = true;
            }
            else if (node.value.compareTo(value) < 0) { //compares value to go left or right
                node = node.right;
            }
            else {
                node = node.left;
            }
        }
        return found;
    }

    public void display(String message) { //public callable method
        System.out.println(message);
        display(this.root);
    }

    private void display(TreeNode node) { //inorder transverse
        if (node != null) {
            display(node.left);
            System.out.println(node.value);
            display(node.right);
        }
    }

    public int numberNodes() { //returns the number of nodes
        return numberOfNodes;
    }

    public int numberLeafNodes() {
        return getLeafNodeCount(root); //get method for leaf nodes
    }

    private int getLeafNodeCount(TreeNode node) {
        if (node == null) { //if null there are no leaf nodes
            return 0;
        }
        else if (node.left == null && node.right == null) {
            return 1; //finds the leaf node
        }
        else {
            return getLeafNodeCount(node.left) + getLeafNodeCount(node.right); //totals the leaf nodes on each side
        }
    }

    public int height() { //get method for height
        return getTreeHeight(root);
    }

    private int getTreeHeight(TreeNode node) {
        if (node == null) {
            return -1; //-1 because root does not count
        }

        int leftHeight = getTreeHeight(node.left); //recursive for each side of the tree
        int rightHeight = getTreeHeight(node.right);

        if (leftHeight > rightHeight) { //returns the greater height
            return leftHeight + 1;
        }
        else {
            return rightHeight + 1;
        }
    }
}
